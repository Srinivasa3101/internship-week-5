puts 'Hello World' 

puts 'Hi' + ' I\'M' + ' srinivasa'

puts 'ruby' "\n" * 5

class S  
    puts 'Just started class S'  
    puts self  
    module M  
      puts 'Nested module S::M'  
      puts self  
    end 
    puts 'Back in the outer level of S'  
    puts self  
  end  


company = '1985'
puts company


puts `dir` #execute linux commands


a = 'hello '  
a<<'world. 
I love this world...' 
puts a


b = 4 + 5
if b < 5
    puts 'less'
else
    puts 'great'
end


puts 'Enter your name'
STDOUT.flush
name = gets.chomp
puts 'Hi ' + name + '
Enter your first number'
STDOUT.flush
num1 = gets.chomp.to_i
puts 'Now, enter your second number'
STDOUT.flush
num2 = gets.chomp.to_i
sum = num1 + num2
puts "Your total is #{sum}"